-------------------------------------------------------------------------------
-- file:	reg_tb.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Banc d'essai pour le composant reg.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library TP_FPGA;
use TP_FPGA.reg;


entity reg_tb is
end entity;


architecture test of reg_tb is

	component reg port(
		D : in std_logic;
		clk : in std_logic;
		rst : in std_logic;
		Q : out std_logic);
	end component;

	signal D_s : std_logic := '0';
	signal clk_s : std_logic := '0';
	signal rst_s : std_logic := '1';
	signal Q_s : std_logic := '0';

begin

reg0 : reg port map(
	D => D_s,
	Q => Q_s,
	clk => clk_s,
	rst => rst_s);

clk_s <= not clk_s after 5 ns;
D_s <= not D_s after 32 ns;
rst_s <= '0' after 9 ns;

end architecture;
