library IEEE;
use		IEEE.std_logic_1164.all;

library TP_FPGA;
use TP_FPGA.all;

entity pgcd_fsm_bench is 
end entity;

architecture test of pgcd_fsm_bench is
    -- Composants utilisés
    component pgcd_fsm is
        port(
            GO, XEQY, XGTY, RST, CLK: in std_logic;
            SEL, LOAD_X, LOAD_Y, SEL_XY, DONE : out std_logic
        );
    end component;

    -- Signaux
    signal CLK_sig    : std_logic := '0';
	signal RST_sig    : std_logic := '1';
	signal GO_sig     : std_logic := '0';
	signal XEQY_sig   : std_logic := '0';
	signal XGTY_sig   : std_logic := '0';
	signal SEL_sig    : std_logic := '0';
	signal LOAD_X_sig : std_logic := '0';
	signal LOAD_Y_sig : std_logic := '0';
	signal SEL_XY_sig : std_logic := '0';
    signal DONE_sig   : std_logic := '0';

    -- Début de l'architecture
begin

    impl : pgcd_fsm port map(
		CLK => CLK_sig,
		RST => RST_sig,
        GO => GO_sig,
        XEQY => XEQY_sig,
        XGTY => XGTY_sig,
        SEL => SEL_sig,
        LOAD_X => LOAD_X_sig,
        LOAD_Y => LOAD_Y_sig,
        SEL_XY => SEL_XY_sig,
        DONE => DONE_sig
    );

    -- Génération des stimulis
    clk_sig <= not clk_sig after 5 ns;
    RST_sig <= '0' after 1 ns;
    GO_sig <= '1' after 24 ns;
    XGTY_sig <= '1' after 54 ns, '0' after 114 ns;
    XEQY_sig <= '1' after 84 ns;

end architecture;


