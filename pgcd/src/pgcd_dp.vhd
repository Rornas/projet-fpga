-------------------------------------------------------------------------------
-- file:	pgcd_dp.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description structurelle de la partie opérative du coposant pgcd.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity pgcd_dp is
    generic (
        N        : natural
    );
    port (
        X	: in  std_logic_vector(N-1 downto 0);
        Y	: in  std_logic_vector(N-1 downto 0);
        SEL	: in  std_logic;
        SEL_XY	: in  std_logic;
        LOAD_X	: in  std_logic;
        LOAD_Y	: in  std_logic;
        CLK	: in  std_logic;
        RST	: in  std_logic;
        OUTPUT	: out std_logic_vector(N-1 downto 0);
        XEQY	: out std_logic;
        XGTY	: out std_logic
    );
end entity;

architecture structural of pgcd_dp is

	component muxn is
		generic (
			N        : natural
		);
		port(
			IN0      : in  std_logic_vector(N-1 downto 0);
			IN1      : in  std_logic_vector(N-1 downto 0);
			SEL      : in  std_logic;
			OUTPUT   : out std_logic_vector(N-1 downto 0)
		);
	end component;

	component regn is
		generic (
			N        : natural
		);
		port(
			CLK	: in std_logic;
			RST	: in std_logic;
			LOAD	: in std_logic;
			D	: in std_logic_vector(N-1 downto 0);
			Q	: out std_logic_vector(N-1 downto 0)
		);
	end component;

	component subn is
		generic (
			N        : natural
		);
		port(
			A        : in  std_logic_vector(N-1 downto 0);
			B        : in  std_logic_vector(N-1 downto 0);
			S        : out std_logic_vector(N-1 downto 0)
		);
	end component;

	component compn is
		generic (
			N        : natural
		);
		port(
			X	: in std_logic_vector(N-1 downto 0);
			Y	: in std_logic_vector(N-1 downto 0);
			XEQY	: out std_logic;
			XGTY	: out std_logic
		);
	end component;

	signal sub_out_s: std_logic_vector(N-1 downto 0);
	signal chan1_s	: std_logic_vector(N-1 downto 0);
	signal chan2_s	: std_logic_vector(N-1 downto 0);
	signal reg1_s	: std_logic_vector(N-1 downto 0);
	signal reg2_s	: std_logic_vector(N-1 downto 0);
	signal A_s	: std_logic_vector(N-1 downto 0);
	signal B_s	: std_logic_vector(N-1 downto 0);

begin

	mux1 : muxn
	generic map(N => N)
	port map(
		IN0      => X,
		IN1      => sub_out_s,
		SEL      => SEL,
		OUTPUT   => chan1_s
	);

	mux2 : muxn
	generic map(N => N)
	port map(
		IN0      => sub_out_s,
		IN1      => Y,
		SEL      => SEL,
		OUTPUT   => chan2_s
	);

	mux3 : muxn
	generic map(N => N)
	port map(
		IN0      => reg2_s,
		IN1      => reg1_s,
		SEL      => SEL_XY,
		OUTPUT   => A_s
	);

	mux4 : muxn
	generic map(N => N)
	port map(
		IN0      => reg1_s,
		IN1      => reg2_s,
		SEL      => SEL_XY,
		OUTPUT   => B_s
	);

	reg1 : regn
	generic map(N => N)
	port map(
		CLK	=> CLK,
		RST	=> RST,
		LOAD 	=> LOAD_X,
		D 	=> chan1_s,
		Q	=> reg1_s
	);

	reg2 : regn
	generic map(N => N)
	port map(
		CLK	=> CLK,
		RST	=> RST,
		LOAD 	=> LOAD_Y,
		D 	=> chan2_s,
		Q	=> reg2_s
	);

	sub : subn
	generic map(N => N)
	port map(
		A	=> A_s,
		B	=> B_s,
		S	=> sub_out_s
	);

	comp : compn
	generic map(N => N)
	port map(
		X	=> reg1_s,
		Y	=> reg2_s,
		XEQY	=> XEQY,
		XGTY	=> XGTY
	);

	OUTPUT <= reg1_s;

end architecture;
