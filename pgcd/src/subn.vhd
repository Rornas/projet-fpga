-------------------------------------------------------------------------------
-- file:	subn.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un soustracteur de deux signaux N bits.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity subn is
    generic (
        N        : natural
    );
    port (
        A        : in  std_logic_vector(N-1 downto 0);
        B        : in  std_logic_vector(N-1 downto 0);
        S        : out std_logic_vector(N-1 downto 0)
    );
end entity subn;

architecture behavioral of subn is
begin
S <= std_logic_vector(unsigned(A) - unsigned(B));
end architecture behavioral;
