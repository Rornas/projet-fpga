-------------------------------------------------------------------------------
-- file:	muxn.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un multiplexeur 2 entrées de  bits.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity muxn is
    generic (
        N        : natural
    );
    port (
        IN0      : in  std_logic_vector(N-1 downto 0);
        IN1      : in  std_logic_vector(N-1 downto 0);
        SEL      : in  std_logic;
        OUTPUT   : out std_logic_vector(N-1 downto 0)
    );
end entity muxn;

architecture behavioral of muxn is
begin

    process(IN0, IN1, SEL)
    begin
        case SEL is
            when '0' => OUTPUT <= IN0;
            when '1' => OUTPUT <= IN1;
	    when others => OUTPUT <= (N-1 downto 0 => 'X');
        end case;
    end process;

end architecture behavioral;
