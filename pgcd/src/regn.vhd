-------------------------------------------------------------------------------
-- file:	regn.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un registre N bits.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity regn is
	generic(
		N : natural
	);
	port(
		CLK	: in std_logic;
		RST	: in std_logic;
		LOAD	: in std_logic;
		D	: in std_logic_vector(N-1 downto 0);
		Q	: out std_logic_vector(N-1 downto 0)
	);
end entity;	

architecture behavioral of regn is
begin
	process (CLK, RST)
	begin
		if RST='1' then
			Q <= (N-1 downto 0 => '0');
		elsif CLK'event and CLK='1' and LOAD='1' then
			Q <= D;
		end if;
	end process;
end architecture;
