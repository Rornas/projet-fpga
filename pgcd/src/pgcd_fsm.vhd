-------------------------------------------------------------------------------
-- file:	pgcd_fsm.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description de la partie contrôle du comosant pgcd sous la forme
--			d'un automate de Moore.
-------------------------------------------------------------------------------

-- librairies
library IEEE;
use IEEE.std_logic_1164.all;

-- entité du controlleur
entity pgcd_fsm is
    port(
        GO, XEQY, XGTY, RST, CLK: in std_logic;
        SEL, LOAD_X, LOAD_Y, SEL_XY, DONE : out std_logic
    );
end entity;

architecture impl of pgcd_fsm is
    -- états de la machine
    type states_t is (START, INIT, TEST, XMINY, YMINX, RES);
    signal state, next_state: states_t;

begin
    -- Calcul de l'état suivant
    process(state, GO, XEQY, XGTY)
    begin
        case state is
            when START =>
                if GO='0' then next_state<=START;
                else next_state<=INIT;
                end if;
            when INIT =>
                next_state<=TEST;
            when TEST =>
                if XGTY='1' and XEQY='1' then next_state<=TEST; --rebouclage
                elsif XGTY='1' and XEQY='0' then next_state<=XMINY;
                elsif XGTY='0' and XEQY='0' then next_state<=YMINX;
                else next_state<=RES;
                end if;
            when XMINY =>
                next_state<=TEST;
            when YMINX =>
                next_state<=TEST;
            when RES =>
                next_state<=RES;  
        end case;
    end process;

    -- Mise à jour de l'état
    process(CLK, RST)
    begin
        if RST='1' then state <= START; --si reset asynchrone (actif haut)
        elsif CLK'event and CLK='1' then state <= next_state; --sur front montant
        end if;
    end process;

    -- Mise à jour des sorties
    process(state)
    begin
        case state is
            when START =>
                SEL<='0'; LOAD_X<='0'; LOAD_Y<='0'; SEL_XY<='0'; DONE<='0';
            when INIT =>
                SEL<='1'; LOAD_X<='1'; LOAD_Y<='1'; SEL_XY<='0'; DONE<='0';
            when TEST =>
                SEL<='0'; LOAD_X<='0'; LOAD_Y<='0'; SEL_XY<='0'; DONE<='0';
            when XMINY =>
                SEL<='0'; LOAD_X<='1'; LOAD_Y<='0'; SEL_XY<='1'; DONE<='0';
            when YMINX =>
                SEL<='0'; LOAD_X<='0'; LOAD_Y<='1'; SEL_XY<='0'; DONE<='0';
            when RES =>
                SEL<='0'; LOAD_X<='0'; LOAD_Y<='0'; SEL_XY<='0'; DONE<='1';
        end case;
    end process;

end architecture;
