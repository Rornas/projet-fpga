-------------------------------------------------------------------------------
-- file:	compn.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un comparateur de signaux à N bits.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity compn is
	generic(
		N : natural
	);
	port(
		X	: in std_logic_vector(N-1 downto 0);
		Y	: in std_logic_vector(N-1 downto 0);
		XEQY	: out std_logic;
		XGTY	: out std_logic
	);
end entity;

architecture behavioral of compn is
begin
	process (X, Y)
	begin
		if X = Y then
			XEQY <= '1';
			XGTY <= '0';
		elsif X > Y then
			XEQY <= '0';
			XGTY <= '1';
		elsif X < Y then
			XEQY <= '0';
			XGTY <= '0';
		end if;
	end process;
end architecture;
