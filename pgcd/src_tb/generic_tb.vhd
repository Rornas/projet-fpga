-------------------------------------------------------------------------------
-- file:	generic_tb.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Banc d'essai pour tester les composants internes de la partie
--			opérative. Pour chaque coposant nous modifions ce banc. Nous
--			n'avons pas concervé de traces des anciennes versions. La 
--			configuration actuelle permet de tester la partie opérative.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library TP_FPGA;
use TP_FPGA.all;

entity generic_tb is
end entity;

architecture test of generic_tb is

	component pgcd_dp is
	generic (
		N        : natural
	);
	port (
		X	: in  std_logic_vector(N-1 downto 0);
		Y	: in  std_logic_vector(N-1 downto 0);
		SEL	: in  std_logic;
		SEL_XY	: in  std_logic;
		LOAD_X	: in  std_logic;
		LOAD_Y	: in  std_logic;
		CLK	: in  std_logic;
		RST	: in  std_logic;
		OUTPUT	: out std_logic_vector(N-1 downto 0);
		XEQY	: out std_logic;
		XGTY	: out std_logic
	);
	end component;

	constant N : natural := 8;

	signal 	X_s	: std_logic_vector(N-1 downto 0);
	signal 	Y_s	: std_logic_vector(N-1 downto 0);
	signal 	SEL_s	: std_logic := '0';
	signal 	SEL_XY_s: std_logic := '0';
	signal 	LOAD_X_s: std_logic := '0';
	signal 	LOAD_Y_s: std_logic := '0';
	signal 	CLK_s	: std_logic := '0';
	signal 	RST_s	: std_logic := '1';
	signal 	OUTPUT_s: std_logic_vector(N-1 downto 0);
	signal 	XEQY_s	: std_logic;
	signal 	XGTY_s	: std_logic;


begin

	inst : pgcd_dp 
	generic map(N => N)
	port map(
		X	=> X_s,
                Y	=> Y_s,
                SEL	=> SEL_s,
                SEL_XY	=> SEL_XY_s,
                LOAD_X	=> LOAD_X_s,
                LOAD_Y	=> LOAD_Y_s,
                CLK	=> CLK_s,
                RST	=> RST_s,
                OUTPUT	=> OUTPUT_s,
                XEQY	=> XEQY_s,
                XGTY	=> XGTY_s
	);

	X_S	 <= "00010010";
	Y_S	 <= "00001100";
	SEL_s	 <= not SEL_s after 40 ns; 
	SEL_XY_s <= not SEL_s after 20 ns;
	LOAD_X_s <= not LOAD_X_s after 15 ns;
	LOAD_Y_s <= not LOAD_X_s;
	CLK_s	 <= not CLK_s after 5 ns;
	RST_s	 <= '0' after 1 ns;

end architecture;
