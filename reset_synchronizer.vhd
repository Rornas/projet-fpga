-------------------------------------------------------------------------------
-- file:	reset_synchronizer.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un synchroniseur de reset basée le flip-flop décrit
--			dans le fichier reg.vhd.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library TP_FPGA;
use TP_FPGA.reg;

entity reset_synchronizer is
	port(
		clk : in std_logic;
		rst : in std_logic;
		rst_sync : out std_logic
	);
end entity;

architecture arch0 of reset_synchronizer is

	component reg is
		port(
			D : in std_logic;
			clk : in std_logic;
			rst : in std_logic;
			Q : out std_logic
		);
	end component;

	signal reg_init : std_logic := '1';
	signal reg_sig  : std_logic := '0';
	signal reg_sync : std_logic := '0';

begin

	reg0 : reg port map(
		clk => clk,
		rst => rst,
		D => reg_init,
		Q => reg_sig);

	reg1 : reg port map(
		clk => clk,
		rst => rst,
		D => reg_sig,
		Q => reg_sync);

	rst_sync <= not reg_sync;

end architecture;
