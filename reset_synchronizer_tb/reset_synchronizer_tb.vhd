-------------------------------------------------------------------------------
-- file:	reset_synchronizer_tb.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Banc d'essai pour le composant reset_synchronizer.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library TP_FPGA;
use TP_FPGA.reset_synchronizer;
use TP_FPGA.reg;

entity reset_synchronizer_tb is
end entity;

architecture test of reset_synchronizer_tb is

	component reg is
		port(
			D: in std_logic;
			clk: in std_logic;
			rst: in std_logic;
			Q: out std_logic
		);
	end component;

	component reset_synchronizer is
		port(
			clk: in std_logic;
			rst: in std_logic;
			rst_sync: out std_logic
		);
	end component;

	signal rst_s : std_logic := '1';
	signal clk_s : std_logic := '0';
	signal D_s : std_logic := '0';
	signal Q_s : std_logic;
	signal rst_sync_s : std_logic;

begin

	reg0 : reg port map(
		clk => clk_s,
		rst => rst_sync_s,
		D => D_s,
		Q => Q_s);

	rst_sync0 : reset_synchronizer  port map(
		clk => clk_s,
		rst => rst_s,
		rst_sync => rst_sync_s);

	clk_s <= not clk_s after 5 ns;
	rst_s <= '0' after 1 ns,
	         '1' after 72 ns,
	         '0' after 77 ns;
	-- D_s <= not D_s after 51 ns;
	D_s <= '1';

end architecture;
