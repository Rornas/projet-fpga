-------------------------------------------------------------------------------
-- file:	reg.vhd
-- author:	Errikos Messara, Quentin Silvestre
-------------------------------------------------------------------------------
-- brief:	Description d'un registre 1 bit ou flip-flop.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity reg is
	port(
		D : in std_logic;
		clk : in std_logic;
		rst : in std_logic;
		Q : out std_logic
	);
end entity;	

architecture dataflow of reg is
begin
	process (clk, rst)
	begin
		if rst='1' then Q <= '0';
		elsif clk='1' and clk'event then
			Q <= D;
		end if;
	end process;
end architecture;
