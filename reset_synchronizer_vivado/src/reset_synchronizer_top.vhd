library IEEE;
use IEEE.std_logic_1164.all;

library TP_FPGA;
use TP_FPGA.reset_synchronizer;

entity reset_synchronizer_top is
	port(
		clk : in std_logic;
		rst : in std_logic;
		led : out std_logic
	);
end entity;

architecture top of reset_synchronizer_top is

	component reset_synchronizer is
		port(
			clk : in std_logic;
			rst : in std_logic;
			rst_sync : out std_logic
		);
	end component;

	signal clk_s : std_logic := '0';
	signal rst_s : std_logic := '1';
	signal led_s : std_logic := '0';

begin

	rst_sync0 : reset_synchronizer port map(
		clk => clk_s;
		rst => rst_s;
		rst_sync => led_s;
	);

end architecture;
